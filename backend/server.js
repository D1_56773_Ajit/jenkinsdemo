const express = require('express')
const cors = require('cors')
const { request } = require('express')

const app = express()
app.use(cors('*'))
app.use(express.json())

// routers
const routerMovie = require('./routes/movie')
app.use('movie', routerMovie)

// port
app.listen(4000, '0.0.0.0', () =>{
    console.log('server started on port 4000')
})