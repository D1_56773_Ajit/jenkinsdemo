//Q1.Containerize MySQL and create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name) table inside that. Movie table will be cretaed through db.sql file.
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/add', (request, response) => {
  const { title, date, time, name } = request.body

  const query = `
    INSERT INTO movie
      (movie_title, movie_release_date, movie_time, director_name)
    VALUES
      ('${title}','${date}','${time}','${name}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/display/:name', (request, response) => {
    const { name } = request.params
    const query = `
      SELECT movie_id, movie_title, movie_release_date, movie_time, director_name
      FROM movie 
      WHERE movie_title = '${name}'
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

  router.put('/update/:id', (request, response) => {
    const { id } = request.params
    const { date, time } = request.body
  
    const query = `
      UPDATE movie
      SET
      movie_release_date = '${date}', 
      movie_time = '${time}'
      WHERE
        movie_id = ${id}
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  
  router.delete('/delete/:id', (request, response) => {
    const { id } = request.params
  
    const query = `
      DELETE FROM movie
      WHERE
        movie_id = ${id}
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  
  module.exports = router

